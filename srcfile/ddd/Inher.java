package ddd;

// Multiple inheritance using four classes
public class Inher {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		D obj = new D();
		obj.cd();
		obj.b();
		obj.a();
		obj.dt();
	}

}

class Ab{
	int a=100;	// a can able to access all the four classes
	void a(){
		System.out.println("a class is executed"+a);
	}
}

class B extends Ab{
	void b(){
		System.out.println("b class is executed"+a);
	}
}

class c extends B{
	void cd(){
		System.out.println("c class is executed"+a);
	}
}

class D extends c{
	void dt(){
		System.out.println("d class is executed"+a);
	}
}
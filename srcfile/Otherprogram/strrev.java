package Otherprogram;
import java.util.Scanner;
class strrev {
	
	    
	    public static void main(String[] args) {

	        Scanner keyboard = new Scanner(System.in);
	        String inputStr;
	        System.out.println("Enter the string to reverse");
	        inputStr = keyboard.nextLine();
	        System.out.println(reverseString(inputStr));
	        
	    }
	    
	    public static String reverseString(String str){
	       if (str.length() == 1) {
	           return str;
	       }
	    return reverseString(str.substring(1)) + str.charAt(0);
	    }
	    
	 }


package src.String;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
public class convert {

	  public static void main(String[] arg) {
		  SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
	       String dateInString = "3-may-1994";
	        try {

	            Date date = formatter.parse(dateInString);
	            System.out.println(date);
	            System.out.println(formatter.format(date));

	        } catch (ParseException e) {
	            e.printStackTrace();
	        }

	    }

	}

package src.String;

public class BuffBuild_Time {
	public static void main(String[] args) {
			
    //------StringBuffer
	
	long startTime1 = System.currentTimeMillis();
	int d=0;
	for (int i = 0; i < 500000; i++) {
		StringBuffer c = new StringBuffer("BigData");

		c.append("Hadoop");
		d=i;

	}
	System.out.println("Number of Iterations:"+d);
	long endTime1 = System.currentTimeMillis();
	System.out.println("The time taken by StringBuffer is: " + (endTime1 - startTime1) + " ms");

	
	//------StringBuilder
    
	
	long startTime2 = System.currentTimeMillis();
	int e=0;
	for (int i = 0; i < 500000; i++) {
		StringBuilder c = new StringBuilder("BigData");

		c.append("Hadoop");
		e=i;

	}
	System.out.println("Number of Iterations:"+e);
	long endTime2 = System.currentTimeMillis();
	System.out.println("The time taken by StringBuilder is: " + (endTime2 - startTime2) + " ms");

}
}
